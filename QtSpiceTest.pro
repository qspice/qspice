#-------------------------------------------------
#
# Project created by QtCreator 2014-03-08T11:52:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtSpiceTest
TEMPLATE = app


SOURCES += main.cpp\
    qspice/qspicesession.cpp \
    qspice/qspicechannel.cpp \
    qspice/qspicehelper.cpp \
    qspice/qspiceobject.cpp \
    qspice/qspicedisplaychannel.cpp \
    qspice/qspiceinputschannel.cpp \
    qspice/qspicemainchannel.cpp \
    qspice/qspicecursorchannel.cpp \
    qmainspicetest.cpp \
    qspice/qspicewidget.cpp

HEADERS  += \
    qspice/qspicesession.h \
    qspice/qspicechannel.h \
    qspice/qspicehelper.h \
    qspice/qspiceobject.h \
    qspice/qspicedisplaychannel.h \
    qspice/qspiceinputschannel.h \
    qspice/qspicemainchannel.h \
    qspice/qspicecursorchannel.h \
    qmainspicetest.h \
    qspice/qspicewidget.h

FORMS    +=

INCLUDEPATH += /usr/include/spice-1
INCLUDEPATH += /usr/include/spice-client-glib-2.0
INCLUDEPATH += /usr/include/glib-2.0
INCLUDEPATH += /usr/lib/x86_64-linux-gnu/glib-2.0/include



unix|win32: LIBS += -lspice-client-glib-2.0
unix|win32: LIBS += -lgobject-2.0
unix|win32: LIBS += -lglib-2.0
unix: LIBS += -lX11


OTHER_FILES +=
