#ifndef QMAINSPICETEST_H
#define QMAINSPICETEST_H

#include <QMainWindow>
#include <QShortcut>
#include <qspice/qspicewidget.h>


class QMainSpiceTest : public QMainWindow
{
    Q_OBJECT
public:
    explicit QMainSpiceTest(QWidget *parent = 0);
    ~QMainSpiceTest();

public:
private:
    QSpiceWidget *spice;
    QShortcut *actFullScreen;


public slots:
    void DisplayResize(const QSize &size);
    void FullScreenTriggered();
    void ConnectClicked();
    void DisconnectClicked();
    void RefreshClicked();
};

#endif // QMAINSPICETEST_H
