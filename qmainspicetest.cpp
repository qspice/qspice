#include "qmainspicetest.h"
#include <QDebug>


QMainSpiceTest::QMainSpiceTest(QWidget *parent) :
    QMainWindow(parent)
{
    setCentralWidget(spice = new QSpiceWidget(this));


    /*
    connect(ui->bnConnect, SIGNAL(clicked()), SLOT(ConnectClicked()));
    connect(ui->bnDisconnect, SIGNAL(clicked()), SLOT(DisconnectClicked()));
    connect(ui->bnRefresh, SIGNAL(clicked()), SLOT(RefreshClicked()));

    spiceSession->setUri("spice://localhost:5900");
    if (spiceSession->Connect())
        qDebug() << "Connected";
    else
        qDebug() << "Connect failed";
    */


    actFullScreen = new QShortcut(QKeySequence(tr("Shift+F11", "View|Full Screen")), this);
    connect(actFullScreen, SIGNAL(activated()), SLOT(FullScreenTriggered()));
    connect(spice, SIGNAL(DisplayResize(QSize)), SLOT(DisplayResize(QSize)));

    if (spice->Connect("spice://localhost:5901"))
        qDebug() << "Connected";
    else
        qDebug() << "Connect failed";

}

QMainSpiceTest::~QMainSpiceTest()
{
}

void QMainSpiceTest::DisplayResize(const QSize &size)
{
    resize(size);
}

void QMainSpiceTest::FullScreenTriggered()
{
    if (isFullScreen())
        setWindowState(Qt::WindowNoState);
    else
        setWindowState(Qt::WindowFullScreen);
}

void QMainSpiceTest::ConnectClicked()
{
    /*
    qDebug() << "Connect Clicked for " << ui->edUri->text() << endl;
    spiceSession->setUri(ui->edUri->text());
    if (spiceSession->Connect())
        qDebug() << "Connected";
    else
        qDebug() << "Connect failed";
    */
}

void QMainSpiceTest::DisconnectClicked()
{
    qDebug() << "Disconnect Clicked";
    //spiceSession->Disconnect();
}


void QMainSpiceTest::RefreshClicked()
{
    //m_Image->repaint();
}

